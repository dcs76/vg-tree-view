package main

import "github.com/vugu/vugu"

type TreeNode struct {
	NodeModel
	IsHovering bool
}

func (c *TreeNode) Init() {
	if c.Current.IconColor == "" { // set a default icon color
		c.Current.IconColor = "RoyalBlue"
	}
	if c.Current.CreateIcon == "" {
		c.Current.CreateIcon = "fas fa-plus"
	}
	if c.Current.UpdateIcon == "" {
		c.Current.UpdateIcon = "fas fa-pen"
	}
	if c.Current.DeleteIcon == "" {
		c.Current.DeleteIcon = "fas fa-eraser"
	}
	if c.Current.PlusMinus == "" {
		c.Current.PlusMinus = "far fa-plus-square"
	}
}

func (c *TreeNode) Compute() {
}

func (c *TreeNode) ClickEventHandler(event vugu.DOMEvent, node *NodeModel) {
	if node.ClickEvent != nil {
		node.ClickEvent(event, node)
	}
}

func (c *TreeNode) DblClickEventHandler(event vugu.DOMEvent, node *NodeModel) {
	if node.DblClickEvent != nil {
		node.DblClickEvent(event, node)
	}
}

func (c *TreeNode) CreateEventHandler(event vugu.DOMEvent, node *NodeModel) {
	if node.CreateEvent != nil {
		node.CreateEvent(event, node)
	}
}

func (c *TreeNode) UpdateEventHandler(event vugu.DOMEvent, node *NodeModel) {
	if node.UpdateEvent != nil {
		node.UpdateEvent(event, node)
	}
}

func (c *TreeNode) DeleteEventHandler(event vugu.DOMEvent, node *NodeModel) {
	if node.DeleteEvent != nil {
		node.DeleteEvent(event, node)
	}
}
