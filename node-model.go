package main

import (
	"log"

	"github.com/vugu/vugu"
)

type TreeEventHanlder func(event vugu.DOMEvent, node *NodeModel)

type NodeModel struct {
	Id            string
	Text          string
	PlusMinus     string
	Icon          string
	IconClosed    string
	IconOpened    string
	IconColor     string
	CreateIcon    string
	UpdateIcon    string
	DeleteIcon    string
	Children      []*NodeModel
	Expanded      bool
	Visible       bool
	IsCRUDVisible bool
	CanCreate     bool
	CanUpdate     bool
	CanDelete     bool
	Type          string
	SubType       string
	Data          interface{} // attach any extra data to the model here

	// event handlers
	ClickEvent    TreeEventHanlder
	DblClickEvent TreeEventHanlder
	CreateEvent   TreeEventHanlder
	UpdateEvent   TreeEventHanlder
	DeleteEvent   TreeEventHanlder

	Current  *NodeModel
	Previous *NodeModel
}

func (c *NodeModel) HasChildren() bool {
	if c.Current != nil {
		log.Print(c.Current.Id)
	}
	res := len(c.Children) > 0
	return res
}

func (c *NodeModel) ToggleNode() {
	log.Println("In NodeModel.ToggleNode()", c.Id)

	c.Expanded = !c.Expanded
	if c.Expanded == true {
		c.PlusMinus = "far fa-minus-square"
		c.Icon = c.IconOpened
	} else {
		c.PlusMinus = "far fa-plus-square"
		c.Icon = c.IconClosed
	}

	for i := 0; i < len(c.Children); i++ {
		c.Children[i].Visible = !c.Children[i].Visible
	}

	// if the branch is collapsed hide everything below it
	if !c.Expanded {
		c.hideChildren(c)
	}
}

func (c *NodeModel) hideChildren(current *NodeModel) {
	if !current.Expanded {
		for _, child := range current.Children {
			child.Visible = false
			child.Expanded = false
			c.PlusMinus = "far fa-plus-square"
			child.Icon = c.IconClosed
			c.hideChildren(child)
		}
	}
}
